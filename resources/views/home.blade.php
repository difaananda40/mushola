@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center mb-3">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Saldo</div>
                <div class="card-body">
                    <h3>Saldo: Rp. {{ number_format($saldo, 0,",",".") }}</h3>
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Laporan</div>
                <div class="card-body">
                    <div class="row mb-4">
                        <div class="col">
                            <h5>Per Hari</h5>
                            <p class="m-0">Pemasukan: <b>Rp. {{ number_format($pemasukan_perhari, 0,",",".")  }}</b></p>
                            <p class="m-0">Pengeluaran: <b>Rp. {{ number_format($pengeluaran_perhari, 0,",",".")  }}</b></p>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col">
                            <h5>Per Bulan</h5>
                            <p class="m-0">Pemasukan: <b>Rp. {{ number_format($pemasukan_perbulan, 0,",",".")  }}</b></p>
                            <p class="m-0">Pengeluaran: <b>Rp. {{ number_format($pengeluaran_perbulan, 0,",",".")  }}</b></p>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col">
                            <h5>Per Tahun</h5>
                            <p class="m-0">Pemasukan: <b>Rp. {{ number_format($pemasukan_pertahun, 0,",",".")  }}</b></p>
                            <p class="m-0">Pengeluaran: <b>Rp. {{ number_format($pengeluaran_pertahun, 0,",",".")  }}</b></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
