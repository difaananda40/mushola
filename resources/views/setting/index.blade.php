@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col">
      @if ($msg = Session::get('success'))
        <div class="alert alert-info alert-block">
          {{ $msg }}
        </div>
      @endif
      <div class="card">
        <div class="card-header">
          Setting
        </div>
        <div class="card-body">
          <form id="form" method="post" action="{{ (!$setting) ? route('setting.store') : route('setting.update') }}">
            @csrf
            @if ($setting)
              @method('PUT')
            @endif
            <div class="form-group">
              <label for="mushola">Nama Mushola / Masjid</label>
              <input
                type="text"
                value="{{ old('mushola') ?? $setting->mushola ?? '' }}"
                class="form-control @error('mushola') is-invalid @enderror"
                name="mushola"
                placeholder="Masukkan nama mushola / masjid"
              />
              @error('mushola')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
              @enderror
            </div>
            <div class="form-group">
              <label for="ketua">Nama Ketua</label>
              <input
                type="text"
                value="{{ old('ketua') ?? $setting->ketua ?? '' }}"
                class="form-control @error('ketua') is-invalid @enderror"
                name="ketua"
                placeholder="Masukkan nama ketua"
              />
              @error('ketua')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
              @enderror
            </div>
            <div class="form-group">
              <label for="bendahara">Nama Bendahara</label>
              <input
                type="text"
                value="{{ old('bendahara') ?? $setting->bendahara ?? '' }}"
                class="form-control @error('bendahara') is-invalid @enderror"
                name="bendahara"
                placeholder="Masukkan nama bendahara"
              />
              @error('bendahara')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
              @enderror
            </div>
            <div class="text-right">
              <button type="submit" class="btn btn-primary">
                @if (!$setting)
                  Submit
                @else
                  Edit
                @endif
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection