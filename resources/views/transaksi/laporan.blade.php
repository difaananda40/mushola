@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">Laporan Transaksi</div>
				<div class="card-body">
					<form method="POST" action="{{ route('transaksi.printLaporan') }}">
						@csrf
						@method('POST')
						<div class="form-group">
							<label for="jenis">Jenis Transaksi</label>
							<select name="jenis" id="jenis" class="form-control @error('jenis') is-invalid @enderror">
									<option value="semua" selected {{ (@old('jenis') === 'semua' ? 'selected' : '') }}>Semua</option>
									<option value="pemasukan" {{ (@old('jenis') === 'pemasukan' ? 'selected' : '') }}>Pemasukan</option>
									<option value="pengeluaran" {{ (@old('jenis') === 'pengeluaran' ? 'selected' : '') }}>Pengeluaran</option>
							</select>
							@error('jenis')
									<span class="invalid-feedback" role="alert">
											<strong>{{ $message }}</strong>
									</span>
							@enderror
						</div>
						<div class="form-group">
							<div class="row">
									<div class="col-6">
											<label for="tanggal_dari">Dari Tanggal</label>
											<input type="text" name="tanggal_dari" class="form-control tgl @error('tanggal_dari') is-invalid @enderror"
													placeholder="Dari Tanggal" value="{{ old('tanggal_dari') }}" />
											@error('tanggal_dari')
													<span class="invalid-feedback" role="alert">
															<strong>{{ $message }}</strong>
													</span>
											@enderror
									</div>
									<div class="col-6">
											<label for="tanggal_sampai">Sampai Tanggal</label>
											<input type="text" name="tanggal_sampai" class="form-control tgl @error('tanggal_sampai') is-invalid @enderror"
													placeholder="Sampai Tanggal" value="{{ old('tanggal_sampai') }}" />
											@error('tanggal_sampai')
													<span class="invalid-feedback" role="alert">
															<strong>{{ $message }}</strong>
													</span>
											@enderror
									</div>
							</div>
						</div>
						@error('message')
							<div class="alert alert-danger alert-block">
								{{ $message }}
							</div>
						@enderror
						<div class="form-group text-right">
							<a href="{{ route('transaksi.index') }}" class="btn btn-danger">Cancel</a>
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(() => {
		$('.tgl').datepicker({
			dateFormat: 'yy-mm-dd'
		});
	});
</script>
@endsection
