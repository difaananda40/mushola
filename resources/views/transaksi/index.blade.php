@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center mb-2">
      <div class="col">
        <h2>Transaksi</h2>
      </div>
      <div class="col-auto">
        @role('super-admin')
        <a class="btn btn-primary" href="{{ url('/transaksi/create') }}">
          Tambah Transaksi
          <i class="fa fa-plus"></i>
        </a>
        @endrole
        <a class="btn btn-primary" href="{{ url('/transaksi/laporan') }}">
          Laporan
          <i class="fa fa-book"></i>
        </a>
      </div>
    </div>
    <div class="row justify-content-center">
      <div class="col">
        @if ($msg = Session::get('success'))
          <div class="alert alert-info alert-block">
            {{ $msg }}
          </div>
        @endif
        <div class="table-responsive">
          <table class="table table-striped" id="tbl">
            <thead class="thead-dark">
              <tr>
                <th scope="col">Tanggal</th>
                <th scope="col">Keterangan</th>
                <th scope="col" class="text-center">Nominal</th>
                <th scope="col" class="text-center">Saldo Terakhir</th>
                <th scope="col">Jenis</th>
                <th scope="col">Opsi</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
</div>
<script>
  $(document).ready( function () {
    $('#tbl').DataTable({
      "processing": true,
      "serverSide": true,
      "order": [[ 0, "desc" ]],
      "ajax": "{{ route('transaksi.getData') }}",
      "columns": [
        { "data": "tanggal" },
        { "data": "keterangan" },
        { "data": "nominal", "class": "text-center", render: (data) => {
          return `<span class="numeric">${data}</span>`
        }},
        { "data": "saldo_terakhir", "class": "text-center", render: (data) => {
          return `<span class="numeric">${data}</span>`
        }},
        { "data": "jenis", render: (data) => {
          let type = '';
          if(data === 'Pemasukan') {
            type = 'badge-primary';
          }
          else if(data === 'Pengeluaran') {
            type = 'badge-danger'
          }
          return `<span class="badge p-2 ${type}">${data}</span>`
        }},
        { render: (data, type, row) => {
            return `
              @role('super-admin')
              <div class="dropdown">
                <button class="btn btn-transparent fa fa-ellipsis-v" type="button" id="dropdownBtn" data-toggle="dropdown" aria-expanded="false">
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownBtn">
                  <form method="post" action="/transaksi/${row.id}">
                      @csrf
                      @method('DELETE')
                      <button type="submit" class="dropdown-item">Hapus</button>
                  </form>
                </div>
              </div>
              @endrole
            `;
        }}
      ],
      "drawCallback": () => {
        new AutoNumeric.multiple('.numeric', {
          currencySymbol : 'Rp. ',
          decimalCharacter : ',',
          digitGroupSeparator : '.',
          allowDecimalPadding: 'false'
        });
      }
    });
  });
</script>
@endsection