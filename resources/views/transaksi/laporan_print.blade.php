<!DOCTYPE html>
<html>
<head>
	<title>Print Laporan</title>
	<style>
    body{
      margin: 0;
      padding: 0;
      width: 100%;
    }

    .surat {
      width: 100%;
    }

    .kop {
      width: 100%;
    }

    .lampiran p {
      text-align: right;
    }

    .lampiran table {
      width: 100%;
      border-collapse: collapse;
    }

    .lampiran table thead {
      background-color: black;
      color: white;
    }

    .lampiran table thead th {
      padding: 10px;
    }

    .lampiran table tbody td {
      border: 1px solid black;
      padding: 5px;
    }

    .center {
      text-align: center;
    }

    .footer {
      margin-top: 40px;
      width: 100%:
      display: block;
      page-break-inside: avoid;
    }

    .footer .jabatan {
      padding-bottom: 50px;
    }
/* 
    .footer .first {
      margin: 0;
      margin-bottom: 15px;
    }

    .footer .wrapper {
      width: 100%;
    }

    .footer .wrapper .left {
      width: 50%;
      display: inline-block;
    }

    .footer .wrapper .right {
      margin-left: -4px;
      width: 50%;
      display: inline-block;
    }

    .footer .left .gelar, .footer .right .gelar {
      margin-bottom: 60px; */
    }
  </style>
</head>
<body>
	<div class="surat">
		<div class="kop" align="center">
      <h3>Laporan Keuangan</h3>
      <h4>Mushola As-Salam Perumahan Permata Hijau</h4>
      <p>
        Dari tanggal <b>{{ $tanggal_dari }}</b> 
        sampai dengan <b>{{ $tanggal_sampai }}</b>
      </p>
			<hr>
		</div>	
		<div class="lampiran">
      <p>Laporan: <b>{{ $jenis }}</b></p>
			<table>
        <thead>
          <tr>
            <th class="center">Tanggal</th>
            <th>Keterangan</th>
            <th class="center">Nominal</th>
            <th class="center">Saldo Terakhir</th>
            <th class="center">Jenis</th>
          </tr>
        </thead>
        <tbody>
        @foreach ($transaksi as $ts)
          <tr>
            <td class="center">{{ $ts->tanggal }}</td>
            <td>{{ $ts->keterangan }}</td>
            <td class="numeric center">Rp. {{ number_format($ts->nominal, 0,",",".") }}</td>
            <td class="numeric center">Rp. {{ number_format($ts->saldo_terakhir, 0,",",".") }}</td>
            <td>{{ $ts->jenis }}</td>
          </tr>
        @endforeach
          <tr>
            <td colspan="2">Total Pemasukan</td>
            <td colspan="3"><b>Rp. {{ number_format($total_pemasukan, 0,",",".") }}</b></td>
          </tr>
          <tr>
            <td colspan="2">Total Pengeluaran</td>
            <td colspan="3"><b>Rp. {{ number_format($total_pengeluaran, 0,",",".") }}</b></td>
          </tr>
          <tr>
            <td colspan="2">Saldo Sekarang</td>
            <td colspan="3"><b>Rp. {{ number_format($saldo, 0,",",".") }}</b></td>
          </tr>
        </tbody>
			</table>
    </div>
    <table class="footer">
      <tr>
        <td colspan="2">Mengetahui,</td>
      </tr>
      <tr>
        <td width="70%" class="jabatan">Ketua</td>
        <td width="30%" class="jabatan">Bendahara</td>
      </tr>
      <tr>
        <td>{{ $setting->ketua }}</td>
        <td>{{ $setting->bendahara }}</td>
      </tr>
    </table>
  </div>
</body>
</html>