@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center mb-2">
      <div class="col">
        <h2>Transaksi Tambah</h2>
      </div>
    </div>
    <div class="row justify-content-center">
      <div class="col">
        @if ($msg = Session::get('success'))
            <div class="alert alert-info alert-block">
              {{ $msg }}
            </div>
        @endif
        <div class="card">
          <div class="card-header">
            Transaksi
          </div>
          <div class="card-body">
            <form id="form" method="post" action="{{ route('transaksi.store') }}">
              @csrf
              <div class="form-group">
                <label for="jenis">Jenis Transaksi</label>
                <select class="form-control @error('jenis') is-invalid @enderror" id="jenis" name="jenis">
                  <option disabled selected hidden>Pilih jenis transaksi</option>
                  <option value="pemasukan" {{ (@old('jenis') === 'pemasukan' ? 'selected' : '') }}>Pemasukan</option>
                  <option value="pengeluaran" {{ (@old('jenis') === 'pengeluaran' ? 'selected' : '') }}>Pengeluaran</option>
                </select>
                @error('jenis')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
              </div>
              <div class="form-group">
                <label for="tanggal">Tanggal</label>
                <input type="text" value="{{ old('tanggal') }}" class="form-control tgl @error('tanggal') is-invalid @enderror" name="tanggal" placeholder="Tanggal">
                @error('tanggal')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
              </div>
              <div class="form-group">
                <label for="keterangan">Keterangan</label>
                <input type="text" value="{{ old('keterangan') }}" class="form-control @error('keterangan') is-invalid @enderror" id="keterangan" name="keterangan" placeholder="Keterangan">
                @error('keterangan')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
              </div>
              <div class="form-group">
                <label for="nominal">Nominal</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">Rp</span>
                  </div>  
                  <input type="text" class="form-control jml @error('nominal') is-invalid @enderror" value="{{ old('nominal') }}" name="nominal" placeholder="Nominal">
                  @error('nominal')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
                </div>
              </div>
              <div class="text-right">
                <a href="{{ route('transaksi.index') }}" class="btn btn-danger">Cancel</a>
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
</div>
<script>
  $(() => {
    const num = new AutoNumeric('.jml', {
      decimalCharacter : ',',
      digitGroupSeparator : '.',
      allowDecimalPadding: 'false'
    });

    $('#form').submit((e) => {
      if($('.jml').val().length > 0) {
        $('.jml').val(num.getNumber())
      }
    })
    
    $('.tgl').datepicker({
      dateFormat: 'yy-mm-dd'
    });
  })
</script>
@endsection