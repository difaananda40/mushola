<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Transaksi extends Model
{
    protected $table = 'transaksi';
    protected $fillable = [
        'tanggal',
        'keterangan',
        'nominal',
        'saldo_terakhir',
        'is_pemasukan',
        'is_pengeluaran',
        'jenis'
    ];
    public $timestamps = true;

    public $appends = ['jenis'];

    protected $casts = [
        'is_pemasukan' => 'boolean',
        'is_pengeluaran' => 'boolean'
    ];

    public function getTanggalAttribute($date) {
        return Carbon::parse($date)->formatLocalized('%d %B %Y');
    }

    public function getJenisAttribute() {
        if($this->is_pemasukan) {
            return 'Pemasukan';
        }
        else if($this->is_pengeluaran) {
            return 'Pengeluaran';
        }
    }

    public function setJenisAttribute($value) {
        if($value == 'pemasukan') {
            $this->attributes['is_pemasukan'] = 1;
            $this->attributes['is_pengeluaran'] = 0;            
        }
        else if($value == 'pengeluaran') {
            $this->attributes['is_pemasukan'] = 0;
            $this->attributes['is_pengeluaran'] = 1;
        }
    }

    public function scopePerHari($query) {
        $query->whereDate('tanggal', Carbon::now());
    }

    public function scopePerBulan($query) {
        $query->whereYear('tanggal', Carbon::now()->year)
              ->whereMonth('tanggal', Carbon::now()->month);
    }

    public function scopePerTahun($query) {
        $query->whereYear('tanggal', Carbon::now()->year);
    }

    public function scopeIsPemasukan($query) {
        $query->where('is_pemasukan', true)
              ->where('is_pengeluaran', false);
    }

    public function scopeIsPengeluaran($query) {
        $query->where('is_pengeluaran', true)
              ->where('is_pemasukan', false);
    }

    public function scopeFilter($query, $request) {
        if($request->has('jenis')) {
            if($request->jenis === 'pemasukan') {
                $query->where('is_pemasukan', true);
            }
            else if($request->jenis === 'pengeluaran') {
                $query->where('is_pengeluaran', true);
            }
        }
        return $query;
    }
}