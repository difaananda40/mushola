<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;
use App\Http\Requests\SettingRequest;

class SettingController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $setting = Setting::first();
        return view('setting.index', compact('setting'));
    }

    public function store(SettingRequest $request) {
        $setting = Setting::get()->count();
        if($setting < 1) {
            Setting::create($request->all());
            return redirect()->route('setting.index')->with('success', 'Setting berhasil ditambah');
        }
    }

    public function update(SettingRequest $request) {
        $setting = Setting::first();
        $setting->update($request->all());
        return redirect()->route('setting.index')->with('success', 'Setting berhasil diubah');
    }
}
