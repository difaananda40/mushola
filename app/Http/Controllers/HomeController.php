<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Keuangan;
use App\Models\Transaksi;
use App\Models\Setting;

class HomeController extends Controller
{
    public function __construct(Keuangan $keuangan, Transaksi $transaksi, Setting $setting)
    {
        $this->middleware('auth');
        $this->keuangan = $keuangan;
        $this->transaksi = $transaksi;
        $this->setting = $setting;
    }

    public function welcome() {
        $mushola = $this->setting->first()->mushola ?? 'Musholla';
        return view('welcome', compact('mushola'));
    }

    public function index()
    {
        return view('home', [
            'saldo' => $this->keuangan->first()->saldo,
            'pemasukan_perhari' => $this->transaksi->perHari()->isPemasukan()->sum('nominal'),
            'pengeluaran_perhari' => $this->transaksi->perHari()->isPengeluaran()->sum('nominal'),
            'pemasukan_perbulan' => $this->transaksi->perBulan()->isPemasukan()->sum('nominal'),
            'pengeluaran_perbulan' => $this->transaksi->perBulan()->isPengeluaran()->sum('nominal'),
            'pemasukan_pertahun' => $this->transaksi->perTahun()->isPemasukan()->sum('nominal'),
            'pengeluaran_pertahun' => $this->transaksi->perTahun()->isPengeluaran()->sum('nominal'),
        ]);
    }
}
