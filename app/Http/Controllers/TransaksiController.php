<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\TransaksiRequest;
use App\Http\Requests\LaporanTransaksiRequest;
use App\Models\Transaksi;
use App\Models\Keuangan;
use App\Models\Setting;
use Illuminate\Support\Facades\DB;
use DataTables;
use Carbon\Carbon;
use PDF;

class TransaksiController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function getData() {
        $ts = Transaksi::get();
        return DataTables::of($ts)->make(true);
    }

    public function index() {
        return view('transaksi.index');
    }

    public function create() {
        return view('transaksi.create');
    }

    public function store(TransaksiRequest $request) {
        $saldo_awal = Keuangan::first()->saldo;
        if($request->jenis === 'pemasukan') {
            $saldo_akhir = $saldo_awal + (int)$request->nominal;
        }
        else if($request->jenis === 'pengeluaran') {
            $saldo_akhir = $saldo_awal - (int)$request->nominal;
        }
        $request->merge(['saldo_terakhir' => $saldo_akhir]);
        
        DB::transaction(function() use($request, $saldo_akhir) {
            Transaksi::create($request->all());
            Keuangan::first()->update([
                'saldo' => $saldo_akhir
            ]);
        });

        return redirect()->route('transaksi.create')->with('success', 'Transaksi berhasil ditambah');
    }

    public function laporan() {
        return view('transaksi.laporan');
    }

    public function printLaporan(LaporanTransaksiRequest $request) {
        $ts = Transaksi::whereDate('tanggal', '>=', $request->tanggal_dari)
                       ->whereDate('tanggal', '<=', $request->tanggal_sampai)
                       ->filter($request)
                       ->orderBy('tanggal', 'asc')
                       ->get();
                       
        $saldo = Keuangan::first()->saldo;

        $setting = Setting::first();

        if(count($ts) < 1) {
            return redirect()->back()->withErrors(['message' => 'Laporan tidak ada!']);
        }

        $data = [
            'transaksi' => $ts, 
            'tanggal_dari' => Carbon::parse($request->tanggal_dari)->formatLocalized('%d %B %Y'),
            'tanggal_sampai' => Carbon::parse($request->tanggal_sampai)->formatLocalized('%d %B %Y'),
            'jenis' => ucfirst($request->jenis),
            'total_pemasukan' => $ts->where('is_pemasukan', true)->sum('nominal'),
            'total_pengeluaran' => $ts->where('is_pengeluaran', true)->sum('nominal'),
            'saldo' => $saldo,
            'setting' => $setting
        ];
        
        $pdf = PDF::loadView('transaksi.laporan_print', $data);
        return $pdf->stream();
    }

    public function destroy($id) {
        $ts = Transaksi::find($id);
        $keuangan = Keuangan::first();
        $saldo_akhir = $keuangan->saldo;
        if($ts->jenis === 'Pemasukan') {
            $saldo_akhir -= $ts->nominal;
        }
        elseif($ts->jenis === 'Pengeluaran') {
            $saldo_akhir += $ts->nominal;
        }
       
        DB::transaction(function() use($ts, $keuangan, $saldo_akhir) {
            $keuangan->update([
                'saldo' => $saldo_akhir
            ]);
            $ts->delete();
        });
        return redirect()->route('transaksi.index')->with('success', 'Transaksi berhasil dihapus');
    }
}

