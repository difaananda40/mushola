<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mushola' => 'required||max:255',
            'ketua' => 'required||max:255',
            'bendahara' => 'required||max:255'
        ];
    }

    public function messages()
    {
        return [
            'mushola.required' => 'Nama mushola/masjid tidak boleh kosong!',
            'ketua.required' => 'Nama ketua tidak boleh kosong!',
            'bendahara.required' => 'Nama bendahara tidak boleh kosong!'
        ];
    }
}
