<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Factory as ValidationFactory;
use App\Models\Keuangan;

class TransaksiRequest extends FormRequest
{
    /**
     * Custom validation for check if saldo is available
     */
    public function __construct(ValidationFactory $validationFactory)
    {
        $validationFactory->extend(
            'saldo_mencukupi',
            function($attribute, $value, $parameters) {
                if($this->jenis === 'pengeluaran') {
                    $saldo = Keuangan::first()->saldo;
                    if($saldo < $value) return false;
                    else return true;
                }
                return true;
            },
            'Saldo tidak mencukupi!'
        );
    }
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tanggal' => 'required|date',
            'keterangan' => 'required|max:255',
            'nominal' => 'required|numeric|saldo_mencukupi|min:1',
            'jenis' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'tanggal.required' => 'Tanggal tidak boleh kosong!',
            'keterangan.required' => 'Keterangan tidak boleh kosong!',
            'nominal.required' => 'Nominal tidak boleh kosong!',
            'nominal.min' => 'Nominal tidak boleh kurang dari :min',
            'jenis.required' => 'Jenis transaksi tidak boleh kosong!'
        ];
    }
}
