<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LaporanTransaksiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tanggal_dari' => 'required|date',
            'tanggal_sampai' => 'required|date',
            'jenis' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'tanggal_dari.required' => 'Dari tanggal tidak boleh kosong!',
            'tanggal_sampai.required' => 'Sampai tanggal tidak boleh kosong!',
            'jenis.required' => 'Jenis transaksi tidak boleh kosong!',
        ];
    }
}
