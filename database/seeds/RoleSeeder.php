<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::insert([
            ['name' => 'admin', 'guard_name' => 'web', 'created_at' => date('Y-m-d')],
            ['name' => 'super-admin', 'guard_name' => 'web', 'created_at' => date('Y-m-d')]
        ]);
    }
}
