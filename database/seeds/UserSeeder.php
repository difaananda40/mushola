<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'difa',
            'password' => bcrypt('difa'), 
        ]);
        $user->assignRole('super-admin');

        $user = User::create([
            'name' => 'dani',
            'password' => bcrypt('dani'), 
        ]);
        $user->assignRole('super-admin');
    
        $user = User::create([
            'name' => 'syahril',
            'password' => bcrypt('syahril'), 
        ]);
        $user->assignRole('admin'); 

        $user = User::create([
            'name' => 'joko',
            'password' => bcrypt('joko'), 
        ]);
        $user->assignRole('admin');
    }
}
