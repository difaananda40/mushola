<?php

use Illuminate\Database\Seeder;
use App\Models\Transaksi;

class TransaksiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('transaksi')->insert([
        //     [
        //         'tanggal' => Date('Y-m-d'),
        //         'keterangan' => 'Beli karpet',
        //         'nominal' => 100000,
        //         'saldo_terakhir' => 100000,
        //         'is_pemasukan' => 0,
        //         'is_pengeluaran' => 1
        //     ],
        //     [
        //         'tanggal' => Date('Y-m-d'),
        //         'keterangan' => 'Sumbangan dari Difa',
        //         'nominal' => 100000,
        //         'saldo_terakhir' => 200000,
        //         'is_pemasukan' => 1,
        //         'is_pengeluaran' => 0
        //     ],
        // ]);
        $datas = [];
        $jenis = ['pemasukan', 'pengeluaran'];
        for($i = 1; $i <= 100; $i++) {
            $randJenis = array_rand($jenis);
            $data = [
                'tanggal' => date('Y-m-d'),
                'keterangan' => $jenis[$randJenis] . ' ' . $i,
                'nominal' => 100000,
                'saldo_terakhir' => 200000,
                'is_pemasukan' => ($randJenis == 0) ? 1 : 0,
                'is_pengeluaran' => ($randJenis == 1) ? 1 : 0
            ];
            array_push($datas, $data);
        }
        Transaksi::insert($datas);
    }
}
