<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@welcome')->name('welcome');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['role:super-admin']], function () {
    // Route::resource('/transaksi', 'TransaksiController');
    Route::get('/transaksi/create', 'TransaksiController@create')->name('transaksi.create');
    Route::post('/transaksi', 'TransaksiController@store')->name('transaksi.store');
    Route::get('/setting', 'SettingController@index')->name('setting.index');
    Route::post('/setting', 'SettingController@store')->name('setting.store');
    Route::put('/setting', 'SettingController@update')->name('setting.update');
});

Route::group(['middleware' => ['role:super-admin|admin']], function () {
    Route::get('/transaksi', 'TransaksiController@index')->name('transaksi.index');
    Route::get('/transaksi/laporan', 'TransaksiController@laporan')->name('transaksi.laporan');
    Route::post('/transaksi/laporan/print', 'TransaksiController@printLaporan')->name('transaksi.printLaporan');
    Route::get('/transaksi/get-data', 'TransaksiController@getData')->name('transaksi.getData');
    Route::delete('/transaksi/{id}', 'TransaksiController@destroy')->name('transaksi.destroy');
});